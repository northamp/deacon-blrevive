# Contributing to Deacon

This project is open to MRs of any kind, but all rights are reserved to modify or outright refuse contributions with or without a reason.

In general please strive to maintain the project's modularity by relying as much as possible on the Cogs system.

## About pictures

The project's `rsc/img` directory contains a few directories themselves containing pictures that are used by the bot in various situations.

They are as follow:

* `denied`: Thumbnails used to reply to an user that tried to use a forbidden command
* `error`: Thumbnails used to reply to an user that hit an error after using a command
* `ok`: Thumbnails used to reply to an user that successfully ran a command
* `event_banners`: Banners used for scheduled events, automatically or not

Do feel free to contribute pictures by forking the project, adding relevant pictures in the appropriate directory and opening a MR.

Please follow the instructions below to ensure they won't have to be modified before being added to the bot:

* Use compressed formats (JPG/WEBP/...) rather than lossless to prevent bloating the project too much. The less space a picture takes the more the project can contain.
* *Thumbnails*: Should be square, and subject should be easily identifiable due to the thumbnails' reduced sizes (though users can click to enlarge them). Preferred resolution would be 500px square max.
* *Event banners*: Discord recommends using 800x400 pictures, though display varies greatly on all devices. In all, please try to aproach a 2.5:1 aspect ratio and test your picture on an event before contributing it.

Offensive or inappropriate pictures are obviously not welcome.
