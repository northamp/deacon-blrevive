#!/usr/bin/env python

"""Deacon's internals
"""

import logging
from pythonjsonlogger.json import JsonFormatter

from typing import Optional, Union
import discord
from discord.ext import commands

from tortoise import Tortoise


class Deacon(commands.Bot):
    def __init__(self, config: dict):
        super().__init__(
            command_prefix=commands.when_mentioned_or('!deacon '),
            status=discord.Status.online,
            intents=discord.Intents.default()
        )
        self.config = config
        self.logger = logging.getLogger(__name__)

    async def on_connect(self):
        db_config = self.config.get('database')
        if not db_config:
            db_url = 'sqlite://deacon.sqlite'
        elif db_config.get('engine', 'sqlite') == 'sqlite':
            db_url = 'sqlite://{path}'.format(
                path=db_config.get('name', 'deacon.sqlite')
            )
        else:
            db_url = '{engine}://{username}:{password}@{hostname}:{port}/{name}'.format(
                engine=db_config.get('engine', 'postgres'),
                username=db_config.get('username'),
                password=db_config.get('password'),
                hostname=db_config.get('hostname'),
                port=db_config.get('port', '5432'),
                name=db_config.get('name')
            )

        try:
            await Tortoise.init(
                db_url=db_url,
                modules={'models': ['utils.models']}
            )
            await Tortoise.generate_schemas()
            self.logger.info('Database connection established successfully.')
        except Exception as e:
            self.logger.critical('Failed to initialize database connection', exc_info=e)
            await self.close()

        # Cogs that don't require a database
        await self.load_extension('cogs.error_handler')
        # ZCure-related tasks, including fetching status
        await self.load_extension('cogs.zcure')
        # Meta stuff, relies on ZCure for some aspects
        await self.load_extension('cogs.meta')
        # Commands and tasks related to singular servers (i.e. status)
        await self.load_extension('cogs.gameservers')
        # Self-updating server browser
        await self.load_extension('cogs.serverbrowser')
        # Activity notifications
        await self.load_extension('cogs.serverwatcher')

    async def on_ready(self):
        self.logger.info(f'Logged in as {self.user}')

    async def close(self):
        self.logger.info('Closing database connection...')
        await Tortoise.close_connections()
        await super().close()

    def run(self):
        if self.config['bot']['log']['format'] == 'json':
            json_formatter = JsonFormatter(
                "{levelname}{asctime}{module}{funcName}{message}{exc_info}",
                style="{",
            )
            discord.utils.setup_logging(level=self.config['bot']['log']['level'] or 'INFO', formatter=json_formatter)
        else:
            discord.utils.setup_logging(level=self.config['bot']['log']['level'] or 'INFO')
        super().run(self.config['bot']['token'], log_handler=None)

    # Helper functions essentially based on https://github.com/Ken-Miles/aidenlib/blob/9a4a760c155c82cbf6b23425993bcb4c7dbce7bd/src/aidenlib/main.py - a MIT-licensed library
    async def get_or_fetch_guild(self, guild_id: int) -> Optional[discord.Guild]:
        """Gets a guild from a bot, if not found, fetches it. If a fetch fails, returns None"""
        guild = self.get_guild(guild_id)
        if guild is None:
            try:
                guild = await self.fetch_guild(guild_id)
                self.logger.debug(f"Fetched guild {guild_id} since it wasn't in the cache")
            except (discord.errors.Forbidden, discord.errors.NotFound):
                return None
        return guild

    async def get_or_fetch_channel(self, channel_id: int, guild: Optional[discord.Guild] = None) -> Optional[Union[discord.abc.GuildChannel, discord.Thread, discord.abc.PrivateChannel]]:
        """Gets a channel from a guild or bot, if not found, fetches it. If a fetch is forbidden/not found, returns None"""
        channel: Optional[Union[discord.abc.GuildChannel, discord.Thread, discord.abc.PrivateChannel]] = None
        if guild is not None:
            channel = guild.get_channel_or_thread(channel_id)
            if channel is None:
                try:
                    channel = await guild.fetch_channel(channel_id)
                    self.logger.debug(f"Fetched guild {channel_id} since it wasn't in the cache")
                except (discord.errors.Forbidden, discord.errors.NotFound):
                    return None
        else:
            channel = self.get_channel(channel_id)
            if channel is None:
                try:
                    channel = await self.fetch_channel(channel_id)
                    self.logger.debug(f"Fetched guild {channel_id} since it wasn't in the cache")
                except (discord.errors.Forbidden, discord.errors.NotFound):
                    return None
        return channel

    async def get_or_fetch_message(self, message_id: int, channel: Union[discord.abc.GuildChannel, discord.Thread, discord.abc.PrivateChannel]) -> Optional[Union[discord.abc.GuildChannel, discord.Thread, discord.abc.PrivateChannel]]:
        """Gets a (partial) message from a channel or DM, if not found, fetches it. If a fetch is forbidden/not found, returns None"""
        message: Optional[discord.Message] = None
        message = channel.get_partial_message(message_id)
        if message is None:
            try:
                message = await channel.fetch_message(message_id)
                self.logger.debug(f"Fetched guild {message_id} since it wasn't in the cache")
            except (discord.errors.Forbidden, discord.errors.NotFound):
                return None
        return message

    async def get_or_fetch_user(self, user_id: int, guild: Optional[discord.Guild] = None) -> Optional[Union[discord.User, discord.Member]]:
        """Gets a user from a guild or bot, if not found, fetches it. If a fetch is forbidden/not found, returns None"""
        user: Optional[Union[discord.User, discord.Member]] = None
        if guild is not None:
            user = guild.get_member(user_id)
            if user is None:
                try:
                    user = await guild.fetch_member(user_id)
                    self.logger.debug(f"Fetched guild {user_id} since it wasn't in the cache")
                except (discord.errors.Forbidden, discord.errors.NotFound):
                    return None
        else:
            user = self.get_user(user_id)
            if user is None:
                try:
                    user = await self.fetch_user(user_id)
                    self.logger.debug(f"Fetched guild {user_id} since it wasn't in the cache")
                except (discord.errors.Forbidden, discord.errors.NotFound):
                    return None
        return user
