#!/usr/bin/env python

"""Launch script for Deacon
"""

import Deacon
import argparse
import json
import os


def parse_env():
    """Parse system envvars to build the config dict"""

    config = {}
    config['bot'] = {}
    config['bot']['token'] = os.getenv('DEACON_TOKEN')
    config['bot']['log'] = {}
    config['bot']['log']['level'] = os.getenv('DEACON_LOG_LEVEL')
    config['bot']['log']['format'] = os.getenv('DEACON_LOG_FORMAT')
    config['zcure'] = {}
    config['zcure']['url'] = os.getenv('DEACON_ZCURE_URL')
    config['database'] = {}
    config['database']['engine'] = os.getenv('DEACON_DATABASE_ENGINE')
    config['database']['username'] = os.getenv('DEACON_DATABASE_USERNAME')
    config['database']['password'] = os.getenv('DEACON_DATABASE_PASSWORD')
    config['database']['hostname'] = os.getenv('DEACON_DATABASE_HOSTNAME')
    config['database']['port'] = os.getenv('DEACON_DATABASE_PORT')
    config['database']['name'] = os.getenv('DEACON_DATABASE_NAME')

    return config


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='A Discord bot able to dial a Blacklight: Revive ZCure master server')
    parser.add_argument('-f', '--configfile', metavar='config.json', nargs='?',
                        default='config.json', help='Use specified configuration file.')
    parser.add_argument('--docker', '--env', action='store_true', help='Get configuration from envvars.')

    args = parser.parse_args()
    if not (args.configfile):
        raise ValueError('"-f" cannot be empty.')

    if not args.docker:
        with open(args.configfile, 'r') as config_file:
            config = json.loads(config_file.read())
    else:
        config = parse_env()

    bot = Deacon.Deacon(config)
    bot.run()
