#!/usr/bin/env python

import numpy as np

# flake8: noqa
# not touching that lol

"""Here be dragons - Python implementation of ZCure's XXTEA encryption/decryption class, courtesy of Ewald"""

class XXXTEA:
    Keys = np.array([4271383519, 3752564771, 1127922790, 1752379389], dtype=np.uint32)
    Iterations = 12
    Delta = np.uint32(2654435769)

    @staticmethod
    def internal(v, k, encrypt=True):
        v = v.copy()
        DELTA = 0x9e3779b9
        MX = lambda sum, y, z, p, e, k: ((z >> 5 ^ y << 2) + (y >> 3 ^ z << 4)) ^ ((sum ^ y) + (k[p & 3 ^ e] ^ z))
        n = len(v)
        rounds = 6 + 52 // n
        if encrypt:
            z = v[-1]
            sum = 0
            for _ in range(rounds):
                sum = (sum + DELTA) & 0xffffffff
                e = sum >> 2 & 3
                for p in range(n):
                    y = v[(p + 1) % n]
                    z = v[p] = (v[p] + MX(sum, y, z, p, e, k)) & 0xffffffff
            return v
        else:
            y = v[0]
            sum = (rounds * DELTA) & 0xffffffff
            for _ in range(rounds):
                e = sum >> 2 & 3
                for p in range(n - 1, -1, -1):
                    z = v[p - 1 if p != 0 else n - 1]
                    y = v[p] = (v[p] - MX(sum, y, z, p, e, k)) & 0xffffffff
                    # Problem with assignment above:  "assignment destination is read-only"
                sum = (sum - DELTA) & 0xffffffff
            return v

    @staticmethod
    def encrypt(buffer: bytearray) -> tuple:
        pad_length = (4 - (len(buffer) % 4)) % 4
        buffer = buffer + (b'\0' * pad_length)
        v = np.frombuffer(buffer, dtype=np.uint32)
        for _ in range(XXXTEA.Iterations):
            v = XXXTEA.internal(v, XXXTEA.Keys)
        return bytearray(v.tobytes()), pad_length

    @staticmethod
    def decrypt(buffer: bytearray) -> bytearray:
        v = np.frombuffer(buffer, dtype=np.uint32)
        for _ in range(XXXTEA.Iterations):
            v = XXXTEA.internal(v, XXXTEA.Keys, False)
        return bytearray(v.tobytes())

class Message:
    def __init__(self) -> None:
        self.padding = 0
        self.message_type = 0
        self.sequence_num = 0
        self.body = bytearray()

    def __init__(self, message_type: np.uint16, sequence_num: np.uint16, body: any) -> None:
        self.padding = 0
        self.message_type = message_type
        self.sequence_num = sequence_num

        if type(body) == str:
            self.body = bytearray(body, encoding='utf-8')
        else:
            self.body = bytearray(body)
    
    @staticmethod
    def decrypt(buffer: bytearray):
        bom = buffer[0:2]
        eom = buffer[-2:]

        if bom != bytes([60, 60]):
            raise ValueError("invalid message bom", bom)
        if eom != bytes([62, 62]):
            raise ValueError("invalid message eom", eom)
        
        messageBytes = XXXTEA.decrypt(buffer[2:-2])
        if messageBytes[0] != 60:
            raise ValueError("invalid message header bom", messageBytes[0])
        if messageBytes[7] != 62:
            raise ValueError("invalid message header eom", messageBytes[7])

        headerBytes = np.frombuffer(messageBytes, dtype=np.uint16, count=6, offset=1)
        padding = headerBytes[0]
        message_type = headerBytes[1]
        sequence_num = headerBytes[2]

        bodyBytes = XXXTEA.decrypt(messageBytes[8:])
        if(len(bodyBytes) - padding < 0):
            raise ValueError(f"invalid padding for {message_type}: {len(bodyBytes)} - {padding}")

        return Message(message_type, sequence_num, bodyBytes) # ignore padding for content


    def encrypt(self) -> bytes:
        bodyBytes, padding = XXXTEA.encrypt(self.body)
        header = np.array([padding, self.message_type, self.sequence_num], dtype=np.uint16)
        headerBytes = bytes([60]) + bytearray(header.tobytes()) + bytes([62])
        messageRawBytes, _ = XXXTEA.encrypt(bytearray(headerBytes + bodyBytes))
        messageBytes = bytes([60, 60]) + messageRawBytes + bytes([62, 62])
        return messageBytes


if __name__ == '__main__':
    # encrypted get file message bytes
    encryptedMessage = bytearray([60,60,154,158,28,57,148,197,178,254,132,136,113,164,24,93,46,158,159,215,98,183,233,31,43,1,158,243,167,154,167,111,183,23,62,62])

    # decrypt message
    message = Message.decrypt(encryptedMessage)
    print(message.message_type, message.sequence_num, message.body.decode('utf-8'))

    # encrypt message
    msg = Message(89, 0, "0:DefaultPublisher.INT")
    enc = msg.encrypt()

    # prove data integrity
    print(enc == encryptedMessage)
