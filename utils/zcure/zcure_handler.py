#!/usr/bin/env python

"""Contains the functions used to poll BL:RE servers
"""

import logging
import aiohttp
import re
from urllib.parse import urljoin
from dataclasses import dataclass, field
from typing import List, Dict, Optional
from datetime import datetime, timezone

from .xxtea import Message


@dataclass
class ServerPlayer:
    """Contains a single player's infos, also suitable for bots. The flesh is weak"""
    Deaths: Optional[int] = None
    Kills: Optional[int] = None
    Name: Optional[str] = None
    Score: Optional[int] = None


@dataclass
class ServerTeam:
    """Contains infos for a specific team"""
    BotCount: Optional[int] = None
    BotList: Optional[List[ServerPlayer]] = None
    PlayerCount: Optional[int] = None
    PlayerList: Optional[List[ServerPlayer]] = None
    TeamIndex: Optional[int] = None
    TeamName: Optional[str] = None
    TeamScore: Optional[int] = None


@dataclass
class ServerMutators:
    """Which mutators are on"""
    DisableDepots: Optional[bool] = None
    DisableElementalAmmo: Optional[bool] = None
    DisableGear: Optional[bool] = None
    DisableHRV: Optional[bool] = None
    DisableHeadShots: Optional[bool] = None
    DisableHealthRegen: Optional[bool] = None
    DisablePrimaries: Optional[bool] = None
    DisableSecondaries: Optional[bool] = None
    DisableTacticalGear: Optional[bool] = None
    HeadshotsOnly: Optional[bool] = None
    StockLoadout: Optional[bool] = None
    HealthModifier: Optional[float] = None
    StaminaModifier: Optional[float] = None


@dataclass
class ServerInfo:
    """Infos for a specific server"""
    Host: str
    Port: int
    Error: Optional[Exception] = None
    ServerName: Optional[str] = None
    BotCount: Optional[int] = None
    GameMode: Optional[str] = None
    GameModeFullName: Optional[str] = None
    GoalScore: Optional[int] = None
    Map: Optional[str] = None
    MaxPlayers: Optional[int] = None
    Mutators: Optional[ServerMutators] = None
    PlayerCount: Optional[int] = None
    Playlist: Optional[str] = None
    RemainingTime: Optional[int] = None
    TeamList: Optional[List[ServerTeam]] = None
    TimeLimit: Optional[int] = None

    @classmethod
    def from_json(cls, data, host, port):
        # Extract relevant fields
        server_name = data.get('ServerName')
        bot_count = data.get('BotCount')
        game_mode = data.get('GameMode')
        game_mode_full_name = data.get('GameModeFullName')
        goal_score = data.get('GoalScore')
        map_name = data.get('Map')
        max_players = data.get('MaxPlayers')
        remaining_time = data.get('RemainingTime')
        playlist = data.get('Playlist')
        time_limit = data.get('TimeLimit')
        player_count = data.get('PlayerCount')

        # Create ServerMutators object
        mutators_data = data.get('Mutators', {})
        mutators = ServerMutators(**mutators_data)

        # Create list of ServerTeam objects
        team_list_data = data.get('TeamList', [])
        team_list = [ServerTeam(**team_data) for team_data in team_list_data]

        # Create and return ServerInfo object
        return cls(
            Host=host,
            Port=port,
            ServerName=server_name,
            BotCount=bot_count,
            GameMode=game_mode,
            GameModeFullName=game_mode_full_name,
            GoalScore=goal_score,
            Map=map_name,
            MaxPlayers=max_players,
            Mutators=mutators,
            PlayerCount=player_count,
            Playlist=playlist,
            RemainingTime=remaining_time,
            TeamList=team_list,
            TimeLimit=time_limit
        )


@dataclass
class ZCureStatus():
    """Dataclass to store ZCure status infos"""
    error: Optional[Exception] = None  # Exception yielded by ZCure on last contact
    last_contact: Optional[datetime] = None  # Timestamp marking the last contact attempt
    game_servers: Dict[int, ServerInfo] = field(default_factory=dict)  # Dict of server data - int is the Server's ID in ZCure

    def get_server_by_id(self, server_id: int) -> Optional['ServerInfo']:
        """Get server info by its ID"""
        return self.game_servers.get(server_id)

    def remove_server_by_id(self, server_id: int):
        """Remove server info by its ID"""
        if server_id in self.game_servers:
            del self.game_servers[server_id]

    def get_server_list(self, sort_by: str):
        """Returns the server list sorted by a specific parameter"""
        if sort_by == 'server_name':
            return sorted(self.game_servers.items(), key=lambda item: item[1].ServerName or "")
        elif sort_by == 'map':
            return sorted(self.game_servers.items(), key=lambda item: item[1].Map or "")
        elif sort_by == 'player_count':
            return sorted(self.game_servers.items(), key=lambda item: item[1].PlayerCount or 0, reverse=True)
        else:
            # Defaults to sorting by server ID if sort_by parameter is empty/invalid
            return sorted(self.game_servers.items(), key=lambda item: item[0])

    def get_total_player_amount(self):
        return sum(server.PlayerCount if server.PlayerCount is not None else 0 for server in self.game_servers.values())

    def get_total_player_capacity(self):
        return sum(server.MaxPlayers if server.MaxPlayers is not None else 0 for server in self.game_servers.values())

    def get_total_servers(self):
        return len(self.game_servers)

    def get_most_populated_server(self) -> Optional[ServerInfo]:
        """Returns the server with the highest player count"""
        if not self.game_servers:
            return None
        return max(self.game_servers.values(), key=lambda server: server.PlayerCount or 0)


class ZCureHandler():
    logger: logging.Logger = logging.getLogger(__name__)  # Logger object
    status = ZCureStatus()

    def __init__(self, zcure_url):
        self.zcure_url = zcure_url
        self.zcure_server_list_endpoint = urljoin(self.zcure_url, '/ZCure/V1/Matchmaking/List')

    @staticmethod
    async def __get_server_list(zcure_server_list_endpoint):
        # https://gitlab.com/blrevive/zcure/-/blob/a3cfcec8e7c31264f786910662ecdeec08a71704/ZCure.API/Protocol/MessageType.cs#L123
        message_type = 21
        # Doesn't matter
        message_seq = 0
        # Gotten from a network capture - region currently doesn't do anything
        message_body = "GameVersion=302;Platform=PC;PublicGame=1;REG=US:0"
        message = Message(message_type, message_seq, message_body)
        http_session = aiohttp.ClientSession()
        try:
            async with http_session.get(zcure_server_list_endpoint, data=message.encrypt()) as resp:
                resp.raise_for_status()
                reslt = await resp.read()
                raw_list = Message.decrypt(reslt).body.decode()
                reg = re.compile(r":(?P<id>\d+):(?P<host>.*?):(?P<port>\d+):\d+:\d+:.*?ServerName=(?P<name>[^,]+),")
                # ZCure returns some bloat before the server list - 4 chars to be precise
                return [m.groupdict() for m in reg.finditer(raw_list[3:])]
        finally:
            await http_session.close()

    @staticmethod
    async def __get_server_info(host, port):
        server_url = f"http://{host}:{port}/server_info"
        http_session = aiohttp.ClientSession()
        try:
            async with http_session.get(server_url) as resp:
                resp.raise_for_status()
                reslt = await resp.json()
                return reslt
        finally:
            await http_session.close()

    async def refresh_server_info(self):
        """Refresh the infos we've got in store"""
        try:
            server_list = await self.__get_server_list(self.zcure_server_list_endpoint)
            self.logger.debug(f"Got the following server list from ZCure: {server_list}")
            self.status.error = None
        except Exception as e:
            self.logger.exception("An error occured while fetching the server list from ZCure", exc_info=True)
            self.status.error = e
            return
        finally:
            self.status.last_contact = datetime.now(tz=timezone.utc)

        for server in server_list:
            try:
                raw_server_info = await self.__get_server_info(server['host'], server['port'])
                self.logger.debug(f"Got the following infos from {server['host']}:{server['port']} (ID #{server['id']}): {raw_server_info}")
                # Tiny sanity check
                if raw_server_info['ServerName'] != server['name']:
                    raise ValueError(f"Wrong server name gotten from {server['host']}:{server['port']} (ID #{server['id']}): expected {server['name']}, got {raw_server_info['ServerName']}")
                self.status.game_servers[int(server['id'])] = ServerInfo.from_json(raw_server_info, server['host'], server['port'])
            except Exception as e:
                self.logger.warning(f"An error occured while fetching infos for server {server['name']} ({server['host']}:{server['port']} ID #{server['id']}): {e}")
                self.status.game_servers[int(server['id'])] = ServerInfo(Host=server['host'], Port=server['port'], Error=e)

        # Clean stale entries
        server_ids = [int(server['id']) for server in server_list]
        stale_entries = [server_id for server_id in self.status.game_servers.keys() if server_id not in server_ids]
        for stale_id in stale_entries:
            self.status.remove_server_by_id(stale_id)

    def get_server_info_by_name(self, server_name: str) -> Optional[ServerInfo]:
        """Retrieve server info by server name"""
        # TODO
