#!/usr/bin/env python

"""Contains various utilities for all cogs to use
"""

from __future__ import annotations

import logging
import os
import random
from datetime import datetime

import discord

from typing import Any, Callable, Coroutine, Optional, Union, Self

logger = logging.getLogger(__name__)


def get_version():
    try:
        from _version import __version__  # pyright: ignore[reportMissingImports]
    except ModuleNotFoundError:
        __version__ = 'dev'

    # Store it in the __version__ var AND return it
    return __version__


class Images():
    logger: logging.Logger = logging.getLogger(__name__)  # Logger object

    def random_picture(self, picture_type: str, discord_file: bool = True):
        """Gets a random picture from `rsc/img` depending on the specified type. Will return as discord.File if discord_file is `True`"""
        picture_path = os.path.join(os.path.curdir, 'rsc', 'img', picture_type)
        picture_file = random.choice(os.listdir(picture_path))
        if discord_file:
            return discord.File(os.path.join(picture_path, picture_file), filename=picture_file)
        else:
            return os.path.join(picture_path, picture_file)


images_helper = Images()


class BaseEmbed(discord.Embed):
    def __init__(self, **kwargs):
        self.set_footer(text=f"Deacon {get_version()}", icon_url="https://gitlab.com/uploads/-/system/group/avatar/7177125/BLReviveLogoNew.png")
        self.timestamp = datetime.now()
        super().__init__(**kwargs)


class CommandPermissions():
    def is_bot_owner():
        async def predicate(interaction: discord.Interaction):
            logger.debug(f"Checking if {interaction.user.id} owns the bot for command {interaction.command.name}")
            return await interaction.client.is_owner(interaction.user)
        return discord.app_commands.check(predicate)


# https://gist.github.com/Soheab/f46fee27498aad4a8962d59b6f0415c6
class SimpleModalWaitFor(discord.ui.Modal):
    def __init__(
        self,
        title: str = "Waiting For Input",
        *,
        check: Optional[Callable[[Self, discord.Interaction], Union[Coroutine[Any, Any, bool], bool]]] = None,
        timeout: float = 30.0,
        input_label: str = "Input text",
        input_max_length: int = 100,
        input_min_length: int = 5,
        input_style: discord.TextStyle = discord.TextStyle.short,
        input_placeholder: Optional[str] = None,
        input_default: Optional[str] = None,
    ):
        super().__init__(title=title, timeout=timeout, custom_id="wait_for_modal")
        self._check: Optional[Callable[[Self, discord.Interaction], Union[Coroutine[Any, Any, bool], bool]]] = check
        self.value: Optional[str] = None
        self.Interaction: Optional[discord.Interaction] = None

        self.answer = discord.ui.TextInput(
            label=input_label,
            placeholder=input_placeholder,
            max_length=input_max_length,
            min_length=input_min_length,
            style=input_style,
            default=input_default,
            custom_id=self.custom_id + "_input_field",
        )
        self.add_item(self.answer)

    async def interaction_check(self, interaction: discord.Interaction) -> bool:
        if self._check:
            allow = await discord.utils.maybe_coroutine(self._check, self, interaction)
            return allow

        return True

    async def on_submit(self, interaction: discord.Interaction) -> None:
        self.value = self.answer.value
        self.interaction = interaction
        self.stop()
