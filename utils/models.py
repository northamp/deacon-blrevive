#!/usr/bin/env python

from tortoise import fields
from tortoise.models import Model


class ServerBrowserEntry(Model):
    message_id = fields.BigIntField(pk=True)
    channel_id = fields.BigIntField()
    guild_id = fields.BigIntField()
    retries = fields.SmallIntField(default=0)
    disabled = fields.BooleanField(default=False)
    disabled_reason = fields.TextField(null=True)

    class Meta:
        table = "serverbrowsers"


class ServerWatcherEntry(Model):
    channel_id = fields.BigIntField(pk=True)
    guild_id = fields.BigIntField()
    role_id = fields.BigIntField(null=True)
    cooldown = fields.SmallIntField(default=30)
    last_ping = fields.DatetimeField(null=True)
    paused = fields.BooleanField(default=False)
    retries = fields.SmallIntField(default=0)
    disabled = fields.BooleanField(default=False)
    disabled_reason = fields.TextField(null=True)

    class Meta:
        table = "serverwatchers"
