FROM python:3.13-slim as builder

COPY requirements.txt /opt/Deacon/requirements.txt
RUN apt update && \
    apt install --no-install-recommends -y build-essential libpq-dev
RUN pip wheel --no-cache-dir --wheel-dir /opt/wheels/ -r /opt/Deacon/requirements.txt

FROM python:3.13-slim

ARG VERSION

COPY --from=builder /opt/wheels /opt/wheels
RUN apt update && \
    apt install --no-install-recommends -y libpq-dev && \
    rm -rf /var/lib/apt/lists/*
RUN pip install --no-cache-dir --upgrade /opt/wheels/* && rm -rf /root/.cache /opt/wheels
RUN adduser --no-create-home --disabled-password --uid 7894 --system runuser
COPY . /opt/Deacon
RUN echo "__version__ = '$VERSION'" > /opt/Deacon/_version.py
RUN chown -R 7894 /opt/Deacon
USER runuser
WORKDIR /opt/Deacon

CMD [ "python", "./launcher.py", "--docker" ]
