# Deacon - A BL:Revive discord bot

This bot can poll a ZCure Master server for various informations, including:

* Each known Blacklight:Revive servers
  * Also their status down to the individual player and their score, as long as it's running the [server-utils](https://gitlab.com/blrevive/modules/server-utils/) module.

It can also answer to some commands, when some prerequisites are met. More about that in [Managing slash commands](#managing-slash-commands), and if you've got suggestions feel free to create an issue and/or post them on Discord (or even open an MR).

## Prerequisites

This bot was made with Python 3.x.

Dependencies are listed in requirements.txt.

A [token](https://github.com/reactiflux/discord-irc/wiki/Creating-a-discord-bot-&-getting-a-token) will be needed to let the bot join your Discord server.

It doesn't need any extra permissions when joining servers as every actions rely on commands.

A PostgreSQL server is required to enable several features; the bot will disable those in case the database settings are left empty.

## Bot setup

The bot is distributed as a docker container, or can be ran directly.

### Without Docker

Clone the repository, then create a config.toml file using the reference at the bottom of this README.

Run the following command to install the required python dependancies:

`pip install -r requirements.txt`

Then run the script using

`python launcher.py`

### With Docker

The Docker version uses environment variables instead of a toml file.

As such, to run the bot you'll need to fill in the following envvars:

* `DEACON_LOG_LEVEL`: Sets the bot's logging verbosity. From less to more verbose: `CRITICAL`/`ERROR`/`WARNING`/`INFO`/`DEBUG`
* `DEACON_LOG_FORMAT`: Sets the bot's logging format. Possible values currently are `json` (for production) or anything else for discord.py's default formatter
* `DEACON_TOKEN`: Your bot's discord token
* `DEACON_ZCURE_URL`: The ZCure server's URL, scheme and port included (i.e. `http://zcure.softsuitlabs.com:6443/`)
* `DEACON_DATABASE_ENGINE`: What database engine should be used, defaulting to `sqlite`. Should support any database Tortoise ORM supports, and `postgres` is dutifully tested
* `DEACON_DATABASE_NAME`: What database name (or filename in case the case of sqlite) should be used. Defaults to `deacon`
* `DEACON_DATABASE_USERNAME`: Username that can write in the database
* `DEACON_DATABASE_PASSWORD`: Password of that user
* `DEACON_DATABASE_HOSTNAME`: Hostname of the database server
* `DEACON_DATABASE_PORT`: Port of the database server

#### Docker-Compose Example

```yaml
version: '3'
services:
  deacon:
    image: registry.gitlab.com/northamp/deacon-blrevive:latest
    restart: always
    environment:
      - TZ=Europe/Berlin
      - DEACON_LOG_LEVEL=INFO
      - DEACON_TOKEN=ALongStringOfNonsense
      - DEACON_ZCURE_URL=http://zcure.softsuitlabs.com:6443/
      - DEACON_DATABASE_ENGINE=postgres
      - DEACON_DATABASE_NAME=deacon
      - DEACON_DATABASE_HOSTNAME=mypsql
      - DEACON_DATABASE_PORT=5432
      - DEACON_DATABASE_USERNAME=deacon
      - DEACON_DATABASE_PASSWORD=deacon
```

#### Kubernetes Example

```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: deacon-config
  namespace: blrevive
  labels:
    app: blrevive
data:
  DEACON_DATABASE_ENGINE: "postgres"
  DEACON_TOKEN: "ALongStringOfNonsense"
  DEACON_LOG_LEVEL: "INFO"
  DEACON_ZCURE_URL: "http://zcure.softsuitlabs.com:6443/"
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: deacon
  namespace: blrevive
  labels:
    app: deacon
spec:
  replicas: 1
  selector:
    matchLabels:
      app: deacon
  template:
    metadata:
      labels:
        app: deacon
    spec:
      containers:
      - name: deacon
        image: registry.gitlab.com/northamp/deacon-blrevive:latest
        envFrom:
          - configMapRef:
              name: deacon-config
        resources:
          requests:
            memory: "128M"
            cpu: "0.25"
          limits:
            memory: "512M"
            cpu: "2"
```

## Configuration file

Here is a reference configuration file:

```json
{
    "bot": {
        "log": {
          "level": "DEBUG",
          "format": "json",
        },
        "token": "ALongStringOfNonsense"
    },
    "database": {
        "name": "deacon",
        "host": "mypsql",
        "port": "5432",
        "user": "deacon",
        "password": "deacon"
    },
    "zcure": {
        "url": "http://blrrevive.ddd-game.de/"
    }
}
```

## Managing slash commands

Slash commands are a bit of a doozy to synchronise with Discord, as the rate limit is reportedly set quite low, especially for global synchronization.

As such, a `sync` text command is available **to the bot owner** (the one that created the application) in order to manage said commands and how they're deployed on the various servers (aka guilds).

It was taken from [this gist](https://gist.github.com/AbstractUmbra/a9c188797ae194e592efe05fa129c57f), which also explains more about the whole situation.

In a nutshell, you want to use `!deacon sync` (in DMs, or followed by a mention of the bot if it's on a server) to synchronize the commands once it is running.
