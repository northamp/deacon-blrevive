#!/usr/bin/env python

"""
Server list embed manager. Needs the ZCure cog loaded and a database
"""

import logging
import discord
from discord.ext import commands, tasks
import asyncio
import datetime

from Deacon import Deacon
from .zcure import ZCure

from utils.models import ServerWatcherEntry
from utils.models import ServerBrowserEntry

from utils.helpers import CommandPermissions
from utils.helpers import BaseEmbed, SimpleModalWaitFor

from asyncpg.exceptions import PostgresConnectionError, ConnectionDoesNotExistError


class ServerWatcher(commands.Cog):
    logger: logging.Logger = logging.getLogger(__name__)  # Logger object
    serverwatcher_command_group = discord.app_commands.Group(name="watcher", description="Commands related to activity notifications")

    def __init__(self, bot: commands.Bot):
        self.bot: Deacon = bot
        self.watch_for_server_activity.start()
        self.watch_for_server_activity.add_exception_type(PostgresConnectionError, ConnectionDoesNotExistError)

    @tasks.loop(seconds=15)
    async def watch_for_server_activity(self):
        zcure: ZCure = self.bot.get_cog("ZCure")
        if zcure.handler.status.error:
            self.logger.warn("Couldn't reach ZCure, skipping a notification beat")
            return

        # Check if any server has more than 2 players
        active_server = None
        for server in zcure.handler.status.get_server_list('player_count'):
            _, server_info = server
            if not server_info.Error and server_info.PlayerCount > 2:
                self.logger.debug(f"Server {server_info.ServerName} has {server_info.PlayerCount} players, notifying...")
                active_server = server_info
                break

        if not active_server:
            self.logger.debug("No active server found, skipping notification and flushing pauses")
            await ServerWatcherEntry.all().update(paused=False)
            return

        # Prepare the notification message
        mesg = BaseEmbed(
            title=f"Activity detected in {active_server.ServerName}!",
            colour=discord.Color.green(),
        )
        mesg.set_author(name='🚨 LFG')
        mesg.add_field(name='Gamemode', value=active_server.GameModeFullName, inline=False)
        mesg.add_field(name='Players', value=f"{active_server.PlayerCount}/{active_server.MaxPlayers}", inline=False)

        # Notify each watcher
        async for watcher in ServerWatcherEntry.all():
            # Unless they're paused/disabled
            if watcher.disabled or watcher.paused:
                continue
            try:
                # Or the cooldown hasn't passed
                if watcher.last_ping is not None and (watcher.last_ping + datetime.timedelta(minutes=watcher.cooldown)) > datetime.datetime.now(datetime.UTC):
                    self.logger.debug(f"Skipping channel {watcher.channel_id} because we pinged it too recently ({watcher.last_ping})")
                    continue

                guild = await self.bot.get_or_fetch_guild(watcher.guild_id)
                channel = await self.bot.get_or_fetch_channel(watcher.channel_id, guild)

                view = discord.ui.View()
                # Check if a ServerBrowserEntry exists for that guild; if it does, add a button with a link to it
                if await ServerBrowserEntry.filter(guild_id=watcher.guild_id).exists():
                    browserEmbed = await ServerBrowserEntry.filter(guild_id=watcher.guild_id).get()
                    item = discord.ui.Button(
                        style=discord.ButtonStyle.blurple,
                        label="Servers status",
                        url=f"https://discord.com/channels/{watcher.guild_id}/{browserEmbed.channel_id}/{browserEmbed.message_id}"
                    )
                    view.add_item(item=item)

                # role_id 0 means pings are disabled
                if watcher.role_id != 0:
                    await channel.send(content=f"<@&{watcher.role_id}>")
                await channel.send(embed=mesg, view=view)
                watcher.last_ping = datetime.datetime.now(datetime.UTC)
                watcher.paused = True
                await watcher.save()
            except Exception:
                # If something goes wrong, log it and increment the retries til it reaches 5
                self.logger.exception(f'Exception encountered notifying about player activity (Channel: {watcher.channel_id} / Guild: {watcher.guild_id}). This is attempt {watcher.retries}/5', exc_info=True)
                if watcher.retries >= 5:
                    self.logger.warn(f'Activity notifications on channel (Channel: {watcher.channel_id} / Guild: {watcher.guild_id}) will be disabled after failing to refresh it {watcher.retries} times')
                    watcher.disabled = True
                    watcher.disabled_reason = "Retries threshold crossed"
                    await watcher.save()
                else:
                    watcher.retries += 1
                    await watcher.save()

    @watch_for_server_activity.before_loop
    async def before_watch_for_server_activity(self):
        await self.bot.wait_until_ready()

        zcure = self.bot.get_cog("ZCure")
        while zcure.handler.status.last_contact is None:
            self.logger.debug("Waiting until the bot has ringed ZCure before enabling notification beats...")
            await asyncio.sleep(2)

    @serverwatcher_command_group.command(name='create')
    async def create_server_watcher_command(self, interaction: discord.Interaction, ping: bool, cooldown: int = 30):
        """Create a new watcher in this channel

        Parameters
        -----------
        ping: bool
            When specified, ping users with the role specified in the watcher
        every: int
            How many minutes to wait between notifications. Defaults to 30
        """
        existing_watcher = await ServerWatcherEntry.filter(channel_id=interaction.channel_id).first()
        if existing_watcher:
            raise FileExistsError('A watcher is already registered in this channel. If you just deleted it, wait around 2 minutes and try again.')

        if not ping:
            await ServerWatcherEntry.create(
                guild_id=interaction.guild.id,
                channel_id=interaction.channel.id,
                role_id=0,
                cooldown=cooldown
            )
            return await interaction.response.send_message('✅ Done!', ephemeral=True)

        guild = await self.bot.get_or_fetch_guild(interaction.guild_id)
        roles = await guild.fetch_roles()

        valid_roles = []

        for role in roles:
            if role.mentionable:
                valid_roles.append(role)

        wait_modal = SimpleModalWaitFor(
            title=f"Waiting for {interaction.user.name}",
            input_label="What role should be pinged?",
            input_placeholder=", ".join([role.name for role in valid_roles])[:100],
            input_min_length=1,
            input_max_length=100,
        )
        await interaction.response.send_modal(wait_modal)
        await wait_modal.wait()
        if wait_modal.value is None:
            return await interaction.followup.send('You did not enter a value in time, try again later', ephemeral=True)

        role: discord.Role = discord.utils.get(valid_roles, name=wait_modal.value)
        if not role:
            return await wait_modal.interaction.response.send_message(f'Role "{wait_modal.value}" not found or cannot be pinged', ephemeral=True)

        try:
            await ServerWatcherEntry.create(
                guild_id=interaction.guild.id,
                channel_id=interaction.channel.id,
                role_id=role.id,
                cooldown=cooldown
            )
            return await wait_modal.interaction.response.send_message('✅ Done!', ephemeral=True)
        except Exception as e:
            return await wait_modal.interaction.response.send_message(f"Error: {e}", ephemeral=True)

    @serverwatcher_command_group.command(name='delete')
    async def delete_server_browser_command(self, interaction: discord.Interaction):
        """Deletes the watcher in this channel"""
        watcher = await ServerWatcherEntry.filter(channel_id=interaction.channel_id).first()
        if not watcher:
            return await interaction.response.send_message('No watcher found in this channel', ephemeral=True)

        await watcher.delete()
        return await interaction.response.send_message('✅ Done!', ephemeral=True)

    @serverwatcher_command_group.command(name='list')
    @CommandPermissions.is_bot_owner()
    async def list_server_browser_command(self, interaction: discord.Interaction):
        """Lists all watchers. Can only be used by the bot owner"""
        watchers = await ServerWatcherEntry.all()

        if not watchers:
            return await interaction.response.send_message('List of watchers:\nNone!', ephemeral=True)

        message = ''
        for watcher in watchers:
            message += f'* Guild ID: {watcher.guild_id} / Channel ID: {watcher.channel_id} / retries: {watcher.retries}\n'

        return await interaction.response.send_message(f'List of watchers:\n{message}', ephemeral=True)


async def setup(bot: commands.Bot):
    await bot.add_cog(ServerWatcher(bot))
