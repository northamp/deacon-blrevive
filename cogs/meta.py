#!/usr/bin/env python

import asyncio
import discord
from discord.ext import commands, tasks
import logging
from datetime import datetime, timezone, MINYEAR

from typing import Literal, Optional

from Deacon import Deacon
from .zcure import ZCure

from utils.helpers import BaseEmbed


class Meta(commands.Cog):
    logger: logging.Logger = logging.getLogger(__name__)  # Logger object
    status_id: Optional[int] = 0  # Status ID to use for the bot - see update_bot_status()
    ready_since: Optional[datetime] = MINYEAR  # Timestamp marking bot's readiness

    def __init__(self, bot: commands.Bot):
        self.bot: Deacon = bot
        self.update_bot_status.start()

    @discord.app_commands.command(name='about', description="Get some bot trivia")
    async def about(self, interaction: discord.Interaction):
        """Just spits out an embed with some information"""

        aboutme = """
        I'm a bot that periodically polls Blacklight: Revive servers.

        [Built with 🐍 and a bit of ❤ by a.lt](https://gitlab.com/northamp/deacon-blrevive/)
        """
        zcure: ZCure = self.bot.get_cog("ZCure")
        mesg = BaseEmbed(title="About me", description=aboutme, colour=discord.Color.blue())
        mesg.add_field(name='Running since', value=self.ready_since.strftime('%Y-%m-%d %H:%M:%S %Z'), inline=False)
        mesg.add_field(name='ZCure URL', value=zcure.handler.zcure_url, inline=False)
        mesg.add_field(name='Last ZCure contact', value=zcure.handler.status.last_contact.strftime('%Y-%m-%d %H:%M:%S %Z'), inline=True)
        mesg.add_field(name='Last error during ZCure contact', value=zcure.handler.status.error, inline=True)
        await interaction.response.send_message(embed=mesg, ephemeral=True)

    @tasks.loop(seconds=5)
    async def update_bot_status(self):
        """Update the bot's discord status depending on whether it manages to reach ZCure or not.
        Online: ZCure is reachable and at least one player is on
        Away: ZCure is reachable but not a single player is online
        Busy: ZCure server is unreachable"""

        zcure: ZCure = self.bot.get_cog("ZCure")
        if zcure.handler.status.error:
            await self.bot.change_presence(status=discord.Status.do_not_disturb, activity=discord.Game(name='Failed to contact ZCure', emoji='⛔'))
        else:
            total_player_amount = zcure.handler.status.get_total_player_amount()
            if total_player_amount > 0:
                largest_server = zcure.handler.status.get_most_populated_server()
                await self.bot.change_presence(
                    status=discord.Status.online,
                    activity=discord.CustomActivity(name=f'{largest_server.PlayerCount} agent{"s" if total_player_amount > 1 else ""} playing {largest_server.GameMode} ({total_player_amount} total)')
                )
            else:
                await self.bot.change_presence(
                    status=discord.Status.idle,
                    activity=discord.CustomActivity(name='All agents on standby')
                )

    @update_bot_status.before_loop
    async def before_update_bot_status(self):
        await self.bot.wait_until_ready()
        zcure: ZCure = self.bot.get_cog("ZCure")
        while zcure.handler.status.last_contact is None:
            self.logger.debug("Waiting until we got the server status before refreshing discord status...")
            await self.bot.change_presence(
                status=discord.Status.do_not_disturb,
                activity=discord.CustomActivity(name="Bot starting...", emoji='⏳')
            )
            await asyncio.sleep(2)
            self.ready_since = datetime.now(tz=timezone.utc)

    @commands.command()
    @commands.is_owner()
    async def sync(
            self, ctx: commands.Context,
            guilds: commands.Greedy[discord.Object],
            spec: Optional[Literal["~", "*", "^"]] = None) -> None:
        """Text command managing the sync of slash commands.
        Taken from https://gist.github.com/AbstractUmbra/a9c188797ae194e592efe05fa129c57f"""
        if not guilds:
            if spec == "~":
                synced = await ctx.bot.tree.sync(guild=ctx.guild)
            elif spec == "*":
                ctx.bot.tree.copy_global_to(guild=ctx.guild)
                synced = await ctx.bot.tree.sync(guild=ctx.guild)
            elif spec == "^":
                ctx.bot.tree.clear_commands(guild=ctx.guild)
                await ctx.bot.tree.sync(guild=ctx.guild)
                synced = []
            else:
                synced = await ctx.bot.tree.sync()

            await ctx.send(
                f"Synced {len(synced)} commands {'globally' if spec is None else 'to the current guild.'}"
            )
            return

        ret = 0
        for guild in guilds:
            try:
                await ctx.bot.tree.sync(guild=guild)
            except discord.HTTPException:
                pass
            else:
                ret += 1

        await ctx.send(f"Synced the tree to {ret}/{len(guilds)}.")


async def setup(bot: commands.Bot):
    await bot.add_cog(Meta(bot))
