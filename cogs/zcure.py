#!/usr/bin/env python

"""
Blacklight:RE server_info API endpoint-related methods for discord stuff
"""

import logging

from discord.ext import commands, tasks

from Deacon import Deacon

from utils.zcure.zcure_handler import ZCureHandler


class ZCure(commands.Cog):
    logger: logging.Logger = logging.getLogger(__name__)  # Logger object

    # Currently unneeded
    # zcure_command_group = discord.app_commands.Group(name="zcure", description="Blacklight: RE servers-related commands")

    def __init__(self, bot: commands.Bot):
        self.bot: Deacon = bot
        self.handler = ZCureHandler(bot.config["zcure"]["url"])
        self.get_servers_info.start()

    @tasks.loop(seconds=10)
    async def get_servers_info(self):
        """Fetch server list from ZCure, then enrich that by querying each servers it returns"""
        try:
            await self.handler.refresh_server_info()
        except Exception as e:
            self.logger.exception("Exception occurred while refreshing the server list", e)
            return

    @get_servers_info.before_loop
    async def before_get_servers_info(self):
        await self.bot.wait_until_ready()


async def setup(bot: commands.Bot):
    await bot.add_cog(ZCure(bot))
