#!/usr/bin/env python

"""
Blacklight:RE server_info API endpoint-related methods for discord stuff
"""

import logging

import discord
from discord.ext import commands

from Deacon import Deacon
from .zcure import ZCure

from utils.helpers import BaseEmbed, SimpleModalWaitFor


class GameServers(commands.Cog):
    logger: logging.Logger = logging.getLogger(__name__)  # Logger object
    server_command_group = discord.app_commands.Group(name="server", description="Blacklight: RE servers-related commands")

    def __init__(self, bot: commands.Bot):
        self.bot: Deacon = bot

    @server_command_group.command(name='status', description="Displays details of a specific server")
    async def server_status_command(self, interaction: discord.Interaction, id: int):
        """Displays details of a specific server

        Parameters
        -----------
        id: int
            ZCure internal ID assigned to the server - get it from `/server list`
        """
        return await interaction.response.send_message(embed=self.generate_server_status(id))

    def generate_server_status(self, id):
        zcure: ZCure = self.bot.get_cog("ZCure")
        if zcure.handler.status.error:
            raise Exception("Couldn't reach ZCure, unable to run the command")

        try:
            server = zcure.handler.status.game_servers[id]
        except KeyError:
            raise ValueError(f"Server with ID {id} wasn't found")

        if server.Error:
            raise Exception("Couldn't reach the server, unable to run the command")

        mesg = BaseEmbed(colour=discord.Color.green(), title=server.ServerName)
        mesg.set_thumbnail(url="https://gitlab.com/uploads/-/system/group/avatar/7177125/BLReviveLogoNew.png")
        mesg.set_author(name=f"Server Status - ZCure ID #{id}")
        mesg.add_field(name='📃Playlist', value=server.Playlist, inline=True)
        mesg.add_field(name='🎲Gamemode', value=server.GameMode, inline=True)
        mesg.add_field(name='🗺Map', value=server.Map, inline=True)
        mesg.add_field(name='🎮Players', value=f"{server.PlayerCount}/{server.MaxPlayers}", inline=True)
        mesg.add_field(name='⏱️Time Left', value=f"{server.RemainingTime // 60}:{(server.RemainingTime % 60):02d}", inline=True)
        mesg.add_field(name='🎯Score', value=f"{server.GoalScore}", inline=True)
        mesg.add_field(name='🤖Bots', value=f"{server.BotCount}", inline=True)
        return mesg

    @server_command_group.command(name='list')
    @discord.app_commands.choices(sort_by=[
        discord.app_commands.Choice(name='Server ID', value='id'),
        discord.app_commands.Choice(name='Players', value='player_count'),
        discord.app_commands.Choice(name='Map', value='map'),
        discord.app_commands.Choice(name='Server name', value='server_name'),
    ])
    async def server_list_command(self, interaction: discord.Interaction, sort_by: str = "player_count"):
        """Lists servers known by ZCure

        Parameters
        -----------
        sort_by: str
            Sort by the specified parameter. Defaults to the ZCure Server ID if empty/invalid
        """
        embed, view = self.generate_server_list()
        return await interaction.response.send_message(embed=embed, view=view)

    def generate_server_list(self) -> tuple[discord.Embed, discord.ui.View]:
        """Builds an embed containing the list of known ZCure servers and their status"""
        zcure: ZCure = self.bot.get_cog("ZCure")
        view = ServerStatusView(self)
        if zcure.handler.status.error:
            return BaseEmbed(title='Server List', colour=discord.Color.red(), description="Couldn't connect to ZCure!"), view

        description = ""
        for server in zcure.handler.status.get_server_list('player_count'):
            server_id, server_info = server
            if server_info.Error:
                description += f"⛔ (ID #{server_id}) **{server_info.Host}:{server_info.Port}** - Failed to get server infos!\n\n"
            else:
                if server_info.PlayerCount > 0:
                    description += "✅ "
                else:
                    description += "💤 "
                description += f"(ID **#{server_id}**) **{server_info.ServerName}** - {server_info.PlayerCount}/{server_info.MaxPlayers} - {server_info.GameMode} on {server_info.Map}\n\n"

        total_player_amount = zcure.handler.status.get_total_player_amount()
        total_player_capacity = zcure.handler.status.get_total_player_capacity()
        total_servers = zcure.handler.status.get_total_servers()
        mesg = BaseEmbed(title=f"Listing {total_player_amount} out of {total_player_capacity} maximum players on {total_servers} different servers", colour=discord.Color.green(), description=description)
        mesg.set_author(name='\U0001F4BB Blacklight: Revive Server List')
        return mesg, view


class ServerStatusView(discord.ui.View):
    def __init__(self, gameservers):
        super().__init__()
        self.value = None
        self.gameservers = gameservers

    @discord.ui.button(label='Server details', style=discord.ButtonStyle.green)
    async def server_details_button(self, interaction: discord.Interaction, button: discord.ui.Button):
        wait_modal = SimpleModalWaitFor(title=f"Waiting for {interaction.user.name}", input_label="Enter the server ID", input_min_length=1, input_max_length=5)
        await interaction.response.send_modal(wait_modal)
        await wait_modal.wait()
        if wait_modal.value is None:
            await interaction.followup.send('You did not enter a value in time, try again later', ephemeral=True)
            return

        try:
            embed = self.gameservers.generate_server_status(int(wait_modal.value))
            await wait_modal.interaction.response.send_message(embed=embed, ephemeral=True)
        except Exception as e:
            await wait_modal.interaction.response.send_message(f"Error: {e}", ephemeral=True)


async def setup(bot: commands.Bot):
    await bot.add_cog(GameServers(bot))
