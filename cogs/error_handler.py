#!/usr/bin/env python

"""
Cog originally based on a gist by EvieePy
https://gist.github.com/EvieePy/7822af90858ef65012ea500bcecf1612
"""

import discord
from discord.ext import commands
import logging

from Deacon import Deacon

from utils.helpers import BaseEmbed, images_helper


class CommandErrorHandler(commands.Cog):
    def __init__(self, bot):
        self.bot: Deacon = bot

    def cog_load(self):
        tree = self.bot.tree
        self._old_tree_error = tree.on_error
        tree.on_error = self.on_app_command_error

    def cog_unload(self):
        tree = self.bot.tree
        tree.on_error = self._old_tree_error

    @commands.Cog.listener()
    async def on_command_error(self, ctx: commands.Context, error: commands.CommandError):
        """The event triggered when an error is raised while invoking a command."""

        # This prevents any commands with local handlers being handled here in on_command_error.
        if hasattr(ctx.command, 'on_error'):
            return

        # No exception to ignore, for now
        ignored = ()

        # Allows us to check for original exceptions raised and sent to CommandInvokeError.
        # If nothing is found. We keep the exception passed to on_command_error.
        error = getattr(error, 'original', error)

        # Anything in ignored will return and prevent anything happening.
        if isinstance(error, ignored):
            return

        # Incorrect or unknown commands get shrugged off, literally
        elif isinstance(error, (commands.CommandNotFound)):
            return await ctx.message.add_reaction('🤷‍♂️')

        # Now that CommandNotFound is handled, we can initiate a new logger dedicated to the command
        logger = logging.getLogger(f'{ctx.command.cog_name}.{ctx.command.qualified_name}')

        # Disabled commands are verbotten
        if isinstance(error, commands.DisabledCommand):
            logger.warn(f'{ctx.author.name} (ID: {ctx.author.id}) used the {ctx.command.qualified_name} command '
                        f'when it was disabled in {ctx.guild.name} (ID: {ctx.guild.id}).')
            return await ctx.send(f'{ctx.command} has been disabled.')

        # If the command's not supposed to be used on DMs, reject it
        elif isinstance(error, commands.NoPrivateMessage):
            try:
                return await ctx.author.send(f'{ctx.command} can not be used in Private Messages.')
            except Exception:
                pass

        # Check to see where it came from...
        elif isinstance(error, (commands.UserInputError, commands.BadArgument)):
            logger.warn(f'{ctx.author.name} (ID: {ctx.author.id}) used the {ctx.command.qualified_name} command '
                        f'with a bad input in {ctx.guild.name if ctx.guild else "DMs"} '
                        f'(ID: {ctx.guild.id if ctx.guild else "N/A"}).')
            return await ctx.send('Error parsing command arguments.')

        # Handle commands that have been denied, letting the user know he can bugger off
        elif isinstance(error, UserWarning) or isinstance(error, commands.NotOwner):
            logger.warn(f'User {ctx.author.name} (ID: {ctx.author.id}) tried to use {ctx.command.qualified_name} '
                        f'in {ctx.guild.name if ctx.guild else "DMs"}.')
            return await ctx.message.add_reaction('⛔')

        # All other Errors not returned come here... And we can just log the default TraceBack.
        logger.error(f'Uncaught exception. User {ctx.author.name} (ID: {ctx.author.id}) '
                     f'tried to use command {ctx.command.qualified_name} which raised exception: {error}')
        return await ctx.message.add_reaction('🐞')

    async def on_app_command_error(self, interaction: discord.Interaction, error: discord.app_commands.AppCommandError):
        """Function fired when a slash-command fails"""
        logger = logging.getLogger(f'{interaction.command.name}')

        arguments = ""
        for argument in interaction.data["options"]:
            # If command is in a group
            if argument["type"] == 1:
                for subargument in argument["options"]:
                    arguments += f'name: {subargument["name"]}/value: {subargument["value"]};'
            else:
                arguments += f'name: {argument["name"]}/value: {argument["value"]};'

        # If it's a check failure
        if isinstance(error, discord.app_commands.CheckFailure):
            logger.warn(f'User {interaction.user.name} (ID: {interaction.user.id}) tried to use command {interaction.command.name} with arguments: ({arguments}) but does not have required permissions')
            thumb = images_helper.random_picture('denied')
            mesg = BaseEmbed(
                title="⛔ Command denied",
                description=f"Tell the bot owner ({await self.bot.fetch_user((await self.bot.application_info()).owner.id)}) if you think this is a bug",
                colour=0xff0000
            )
            mesg.set_thumbnail(url=f"attachment://{thumb.filename}")
            mesg.add_field(name='Command/Group', value=interaction.data["name"], inline=False)
            for argument in interaction.data["options"]:
                if argument["type"] == 1:
                    mesg.add_field(name='Command', value=argument["name"], inline=True)
                    for subargument in argument["options"]:
                        mesg.add_field(name=subargument["name"], value=subargument["value"], inline=True)
                else:
                    mesg.add_field(name=argument["name"], value=argument["value"], inline=True)

            mesg.add_field(name='Error', value='You do not have permission to run this command', inline=False)
            return await interaction.response.send_message(file=thumb, embed=mesg, ephemeral=True)

        # Might be possible to improve that
        logger.exception(f'Uncaught exception. User {interaction.user.name} (ID: {interaction.user.id}) tried to use command {interaction.command.name} with arguments: ({arguments}) which raised exception: {error}', exc_info=True)

        # Pick a random thumbnail from the appropriate directory
        thumb = images_helper.random_picture('error')

        # Prepare the embed
        mesg = BaseEmbed(
            title="💥 An error happened",
            # Okay that's super wonky, FIXME sometime
            description=f"Send those information to the bot owner ({await self.bot.fetch_user((await self.bot.application_info()).owner.id)}) if you think this is a bug",
            colour=0xffa500
        )
        mesg.set_thumbnail(url=f"attachment://{thumb.filename}")
        mesg.add_field(name='Command/Group', value=interaction.data["name"], inline=False)
        for argument in interaction.data["options"]:
            if argument["type"] == 1:
                mesg.add_field(name='Command', value=argument["name"], inline=True)
                for subargument in argument["options"]:
                    mesg.add_field(name=subargument["name"], value=subargument["value"], inline=True)
            else:
                mesg.add_field(name=argument["name"], value=argument["value"], inline=True)

        mesg.add_field(name='Exception', value=repr(error), inline=False)
        return await interaction.response.send_message(file=thumb, embed=mesg, ephemeral=True)


async def setup(bot: commands.Bot):
    await bot.add_cog(CommandErrorHandler(bot))
