#!/usr/bin/env python

"""
Server list embed manager. Needs the ZCure cog loaded and a database
"""

import logging
import discord
from discord.ext import commands, tasks
import asyncio

from Deacon import Deacon
from .gameservers import GameServers
from .zcure import ZCure

from utils.helpers import CommandPermissions
from utils.models import ServerBrowserEntry

from asyncpg.exceptions import PostgresConnectionError, ConnectionDoesNotExistError


class ServerBrowser(commands.Cog):
    logger: logging.Logger = logging.getLogger(__name__)  # Logger object
    serverbrowser_command_group = discord.app_commands.Group(name="serverbrowser", description="Commands related to the self-updating list of servers")

    def __init__(self, bot: commands.Bot):
        self.bot: Deacon = bot
        self.refresh_embeds.add_exception_type(PostgresConnectionError, ConnectionDoesNotExistError)
        self.refresh_embeds.start()

    @tasks.loop(seconds=30)
    async def refresh_embeds(self):
        """Updates server list embeds managed by the bot"""
        embeds = await ServerBrowserEntry.all()

        if not embeds:
            self.logger.debug('No embeds found in database, skipping update')
            return

        gameservers: GameServers = self.bot.get_cog("GameServers")
        new_message, new_view = gameservers.generate_server_list()

        for embed in embeds:
            if embed.disabled:
                self.logger.debug(f'Skipping disabled embed (Message: {embed.message_id} / Channel: {embed.channel_id} / Guild: {embed.guild_id})')
                continue
            try:
                guild = await self.bot.get_or_fetch_guild(embed.guild_id)
                channel = await self.bot.get_or_fetch_channel(embed.channel_id, guild)
                message = await self.bot.get_or_fetch_message(embed.message_id, channel)
                await message.edit(embed=new_message, view=new_view)
                self.logger.debug(f'Updated embed successfully (Message: {embed.message_id} / Channel: {embed.channel_id} / Guild: {embed.guild_id})')
                embed.retries = 0
                await embed.save()
            except Exception:
                self.logger.exception(f'Exception encountered while updating embed (Message: {embed.message_id} / Channel: {embed.channel_id} / Guild: {embed.guild_id}). This is attempt {embed.retries}/5', exc_info=True)
                if embed.retries >= 5:
                    self.logger.warn(f'Embed (Message: {embed.message_id} / Channel: {embed.channel_id} / Guild: {embed.guild_id}) will be disabled after failing to refresh it {embed.retries} times')
                    embed.disabled = True
                    embed.disabled_reason = "Retries threshold crossed"
                    await embed.save()
                else:
                    embed.retries += 1
                    await embed.save()

    @refresh_embeds.before_loop
    async def before_refresh_embeds_info(self):
        await self.bot.wait_until_ready()

        zcure: ZCure = self.bot.get_cog("ZCure")
        while zcure.handler.status.last_contact is None:
            self.logger.debug("Waiting until the bot has ringed ZCure before refreshing embeds...")
            await asyncio.sleep(2)

    @serverbrowser_command_group.command(name='create')
    async def create_server_browser_command(self, interaction: discord.Interaction):
        """Creates a server browser embed in a channel; defaults to the one it is ran in"""
        existing_message = await ServerBrowserEntry.filter(guild_id=interaction.guild.id).first()
        if existing_message:
            channel = await self.bot.get_or_fetch_channel(existing_message.channel_id, interaction.guild)
            message = await self.bot.get_or_fetch_message(existing_message.message_id, channel)
            raise FileExistsError(f'A server browser embed is already registered for this guild: {message.jump_url} If it does not exist, wait around 2 minutes and try again.')

        gameservers: GameServers = self.bot.get_cog("GameServers")
        new_message, new_view = gameservers.generate_server_list()
        sent_message = await interaction.channel.send(embed=new_message, view=new_view)

        try:
            await ServerBrowserEntry.create(guild_id=interaction.guild.id, channel_id=interaction.channel.id, message_id=sent_message.id)
        except Exception as e:
            await sent_message.delete()
            raise e

        return await interaction.response.send_message('✅ Done!', ephemeral=True)

    @serverbrowser_command_group.command(name='delete')
    async def delete_server_browser_command(self, interaction: discord.Interaction):
        """Deletes the server browser embed in this guild"""
        existing_message = await ServerBrowserEntry.filter(guild_id=interaction.guild.id).first()
        if not existing_message:
            return await interaction.response.send_message('No server browser embed found for this guild.', ephemeral=True)

        channel = await self.bot.get_or_fetch_channel(existing_message.channel_id, interaction.guild)
        message = await self.bot.get_or_fetch_message(existing_message.message_id, channel)
        await message.delete()
        await existing_message.delete()
        return await interaction.response.send_message('✅ Done!', ephemeral=True)

    @serverbrowser_command_group.command(name='list')
    @CommandPermissions.is_bot_owner()
    async def list_server_browser_command(self, interaction: discord.Interaction):
        """Lists all server browser embeds managed by the bot. Can only be used by the bot owner"""
        embeds = await ServerBrowserEntry.all()

        if not embeds:
            return await interaction.response.send_message('List of managed embeds:\nNone!', ephemeral=True)

        message = ''
        for embed in embeds:
            message += f'* Guild ID: {embed.guild_id} / Channel ID: {embed.channel_id} / Message ID: {embed.message_id} / retries: {embed.retries}\n'

        return await interaction.response.send_message(f'List of managed embeds:\n{message}', ephemeral=True)


async def setup(bot: commands.Bot):
    await bot.add_cog(ServerBrowser(bot))
